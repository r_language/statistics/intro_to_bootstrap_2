# Second_Intro_to_bootstrap



In the R file provided there are different methods that are used :


- Calculation of the confidence interval with bootstrap on iris data (ggplot, qqplot, Shapiro-Wilk test)


- Bootstrap estimation with boot package (boot, plot)


- Test of the difference of two means (student test, boot)


- Confidence interval on the ratio between two means (boot)


- Example of regression with nuclear data : bootstrap with error sampling (glm, diag, predict, boot, sort)


- Parametric bootstrap with air conditioning data (boot)


- Evaluation of the performance of a predictive model by bootstrap (sample)


- Example of bootstrap with package caret and supervized classification (trainControl, train)


- Random forest (randomForest)


- Linear regression on mtcars data (lm, coef, sqrt, rstudent, predict)


- Error sampling using mtcars data and lm_mtcars the model that we have fitted before. Bootstrap of the errors on lm_mtcars will allow to obtain an approximative distribution of the coefficients estimator (matrix, sample)


- Test to know the variable benefit (lm, anova, coef, sqrt, rstudent, predict, plot)


- Confidence intervals with logistic regression. To execute this part you will need to change the path to load the file urine.dat that is provided in the Second_Intro_to_bootstrap (glm, coef, residuals, predict)


- Error sampling with urine data. This is the same file used above (urine.dat) (glm, coef, residuals, matrix rbind)



Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021


