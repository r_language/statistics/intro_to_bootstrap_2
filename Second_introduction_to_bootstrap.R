# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021

#######################  Second introduction to bootstrap 

# Calculate the confidence interval with bootstrap on iris data
# We'll determine the confidence interval at 95% on the mean

# Load iris data
data(iris)

# See the length of the Sepal column in the iris dataframe
iris$Sepal.Length

# Histogram of iris$Sepal.Length with frequency on the y axis and values on the x axis
hist(iris$Sepal.Length)


# Needed library to plot
library(ggplot2)

# Approximation of the density with a kernel estimator
# approximated density on the y axis and values on the x axis
ggplot(iris, aes(x=Sepal.Length)) +
  geom_density(fill = "lightblue",alpha=0.5) +
  expand_limits(x = c(3,9))


# We see that the sample probably doesn't follow a normal distribution so we can make some tests

# Needed library
library("car")

# qqplot test
qqPlot(iris$Sepal.Length)
# We see that the points aren't very aligned on the straight line so it means that there is a deviation

# We can apply a Shapiro-Wilk normality test
shapiro.test(iris$Sepal.Length)


# Result
# data:  iris$Sepal.Length
# W = 0.97609, p-value = 0.01018

# So we reject the normality hypothesis with an alpha risk at 5%





####################################################

# Bootstrap estimation with boot package
# Boot allows to resampling (empirical and parametric)
# Needed library
library(boot)


# Create function called mean.boot
mean.boot <- function(d, i){
  d2 <- d[i]
  return(mean(d2))
}


# Create variable iris.boot using mean.boot function
iris.boot <- boot(iris$Sepal.Length, mean.boot, R = 10000)

# Plot the variable iris.boot (histogram and quantiles of standard normal)
plot(iris.boot)


# Use of boot.ci on the variable iris.boot
boot.ci(iris.boot)


# Result
# Intervals : 
# Level      Normal              Basic         
# 95%   ( 5.713,  5.973 )   ( 5.711,  5.971 )  

# Level     Percentile            BCa          
# 95%   ( 5.716,  5.976 )   ( 5.717,  5.977 )  
# Calculations and Intervals on Original Scale






##################################################################

# Test of the difference of two means


# We use the gravity data and we compare 2 groups of people for the variable g

# Load data
data(gravity)

# Create variable called grav1
grav1 <- gravity[as.numeric(gravity[,2]) >= 7,]

# Summary of the variable grav1
summary(grav1)

# Result of the summary
# g             series  
# Min.   :64.00   7      :13  
# 1st Qu.:77.25   8      :13  
# Median :79.00   1      : 0  
# Mean   :78.96   2      : 0  
# 3rd Qu.:81.75   3      : 0  
# Max.   :86.00   4      : 0  
# (Other): 0 



# Student test on grav1
t.test(g ~ series, grav1)

# Result
# data:  g by series
# t = -1.7679, df = 21.62, p-value = 0.09118
# alternative hypothesis: true difference in means between group 7 and group 8 is not equal to 0
# 95 percent confidence interval:
#  -6.1882505  0.4959428
# sample estimates:
#  mean in group 7 mean in group 8 
#        77.53846        80.38462 



# Test between the two means
diff.means <- function(d, f)
{ n <- nrow(d)
gp1 <- which(d$series == 7)
m1 <- sum(d[gp1,1] * f[gp1])/sum(f[gp1])
m2 <- sum(d[-gp1,1] * f[-gp1])/sum(f[-gp1])
ss1 <- sum(d[gp1,1]^2 * f[gp1]) - (m1 * m1 * sum(f[gp1]))
ss2 <- sum(d[-gp1,1]^2 * f[-gp1]) - (m2 * m2 * sum(f[-gp1]))
c(m1 - m2, (ss1 + ss2)/(sum(f) - 2))
}

# Create variable grav.boot with the use of diff.means function
grav.boot = boot(grav1, diff.means, R = 999, stype = "f", strata = grav1[,2])

# See grav.boot result
grav.boot

# Result
# STRATIFIED BOOTSTRAP


# Call:
#  boot(data = grav1, statistic = diff.means, R = 999, stype = "f", 
#       strata = grav1[, 2])


# Bootstrap Statistics :
#  original      bias    std. error
# t1* -2.846154 -0.07676908    1.537424
# t2* 16.846154 -1.09325351    6.808422


# We can obtain the confidence interval bootstraped
boot.ci(grav.boot)

# Result

## BOOTSTRAP CONFIDENCE INTERVAL CALCULATIONS
## Based on 999 bootstrap replicates
##
## CALL :
## boot.ci(boot.out = grav.boot)
##
## Intervals :
## Level Normal Basic Studentized
## 95% (-5.783,  0.244 )   (-5.615,  0.462 )   (-7.052, -0.058 )  
##
## Level Percentile BCa
## 95% (-6.154, -0.077 )   (-6.633, -0.462 ) 
## Calculations and Intervals on Original Scale


# The confidence interval with student test doesn't contain the value 0 so we reject the egality hypothesis on the two means with an alpha risk at 5%









###########################################################################


# Confidence interval on the ratio between two means
# Use of the city data

# Create function called ratio
ratio <- function(d, w) sum(d$x * w)/sum(d$u * w)

# Use boot on the city data with the function ratio
boot(city, ratio, R = 999, stype = "w")

# Result
# ORDINARY NONPARAMETRIC BOOTSTRAP


# Call:
#  boot(data = city, statistic = ratio, R = 999, stype = "w")


# Bootstrap Statistics :
#  original     bias    std. error
# t1* 1.520313 0.03866209   0.2278844

# The operation is done with a weight vector w which sum equals 1








##################################################################

# Example of regression with nuclear data : bootstrap with error sampling

# Extraction of the columns of interest that we store in nuke variable
nuke <- nuclear[, c(1, 2, 5, 7, 8, 10, 11)]

# Apply glm function to nuke data (linear regression on the log of the cost)
# We store the result in nuke.lm variable
nuke.lm <- glm(log(cost) ~ date+log(cap)+ne+ct+log(cum.n)+pt, data = nuke)

# Show results
nuke.lm 

# Results


# Call:  glm(formula = log(cost) ~ date + log(cap) + ne + ct + log(cum.n) + 
#           pt, data = nuke)

# Coefficients:
#  (Intercept)         date     log(cap)           ne  
#     -13.26031      0.21241      0.72341      0.24902  
# ct   log(cum.n)           pt  
# 0.14039     -0.08758     -0.22610  

# Degrees of Freedom: 31 Total (i.e. Null);  25 Residual
# Null Deviance:	    4.428 
# Residual Deviance: 0.6337 	AIC: -18.69




# Calculation of jackknife residuals of the deviance
nuke.diag <- glm.diag(nuke.lm)



# renormalization and recentring of the residuals
nuke.res <- nuke.diag$res * nuke.diag$sd
nuke.res <- nuke.res - mean(nuke.res)

# res : vector of the jackknife residuals of the deviance (leave-one-out)
# sd : estimation of the standard deviation for the gaussian family


# We define a new dataset with data, standardized residuals and ajusted values
nuke.data <- data.frame(nuke, resid = nuke.res, fit = fitted(nuke.lm))

# Example with usine 32 and a date of 73.00
new.data <- data.frame(cost = 1, date = 73.00, cap = 886, ne = 0,
                       ct = 0, cum.n = 11, pt = 1)

# Create variable new.fit using predict on nuke.lm variable
new.fit <- predict(nuke.lm, new.data)


# Create nuke.fun to calculate statistics
nuke.fun <- function(dat, inds, i.pred, fit.pred, x.pred)
{
  lm.b <- glm(fit+resid[inds] ~ date+log(cap)+ne+ct+log(cum.n)+pt,
              data = dat)
  pred.b <- predict(lm.b, x.pred)
  c(coef(lm.b), pred.b - (fit.pred + dat$resid[i.pred]))
}



# Create variable nuke.boot using boot on nuke.data
nuke.boot <- boot(nuke.data, nuke.fun, R = 999, m = 1,
                  fit.pred = new.fit, x.pred = new.data)

# m=1 means that one prediction has to be done for each bootstrap sample


# Show nuke.boot variable
nuke.boot

# Result
# ORDINARY NONPARAMETRIC BOOTSTRAP


# Call:
#  boot(data = nuke.data, statistic = nuke.fun, R = 999, m = 1, 
#       fit.pred = new.fit, x.pred = new.data)


# Bootstrap Statistics :
#  original        bias    std. error
# t1* -12.99073659 -0.1481459139  3.20097343
# t2*   0.20863596  0.0021190710  0.04467187
# t3*   0.72313216 -0.0001300244  0.12210738
# t4*   0.25007667 -0.0033383308  0.07719569
# t5*   0.13323536  0.0068366693  0.06200076
# t6*  -0.09055307  0.0018905939  0.04206779
# t7*  -0.22972664 -0.0011867135  0.11244787
# t8*  -0.11102609  0.1068551695  0.29094087



# The prediction variance can be estimated by bootstrap

mean(nuke.boot$t[, 8]^2)
# Result 
# 0.08457926


# The prediction interval at 95% by bootstrap is 
new.fit - sort(nuke.boot$t[, 8])[c(975, 25)]
# Result
# 6.158750 7.294155




#######################################################################




# Parametric bootstrap with air conditioning data

# Test that the true value of index is 1 (data comes from an exponential distribution)
# vs hypothesis that data comes from a gamma distribution with an index different than 1


# Create function called air.fun (likelihood)
air.fun <- function(data) {
  
  ybar <- mean(data$hours)
  
  para <- c(log(ybar), mean(log(data$hours)))
  
  ll <- function(k) {
    if (k <= 0) 1e200 else lgamma(k)-k*(log(k)-1-para[1]+para[2])
    }
  
  khat <- nlm(ll, ybar^2/var(data$hours))$estimate
  c(ybar, khat)
}
  

# With parametric bootstrap we have to provide a function that allows to generate data
# Here we generate data with null hypothesis : hypothesis of an exponential law

air.rg <- function(data, mle) {
  # Function to generate random exponential variates.
  # mle will contain the mean of the original data
  out <- data
  out$hours <- rexp(nrow(out), 1/mle)
  out
}



# Create variable air.boot using boot function
# mle = mean(aircondit$hours) is the estimator of the likelihood max (of the exponential law parameter)
air.boot <- boot(aircondit, air.fun, R = 999, sim = "parametric",
                 ran.gen = air.rg, mle = mean(aircondit$hours))

# Show result
air.boot

# Results
# PARAMETRIC BOOTSTRAP


# Call:
#   boot(data = aircondit, statistic = air.fun, R = 999, sim = "parametric", 
#       ran.gen = air.rg, mle = mean(aircondit$hours))


# Bootstrap Statistics :
#  original    bias    std. error
# t1* 108.0833333 1.0899327  32.8205474
# t2*   0.7064927 0.5745345   0.6246494

# For the estimated sample we look if on average the bootstrap estimator of k
# (generated with H0 : data follow an exponential law k=1) is closer to 1 than the obtained estimator on the initial data


# Critical bootstrap probability 
sum(abs(air.boot$t[,2]-1) > abs(air.boot$t0[2]-1))/(1+air.boot$R)
# Result
# 0.46

# Value of the estimated parameter on the initial data
air.boot$t0[2]
# Result
# 0.7064927

#
air.fun(aircondit)[2]
# Result
# 0.7064927







#######################################################################

# Evaluation of the performance of a predictive model by bootstrap


dplyr::n_distinct(sample(100000,replace = TRUE))/100000
# Result
# 0.63179
# So 1/3 of indexes are present and 2/3 of indexes are absent


# Look how many times each value is drawn
table(factor(sample(20, replace = TRUE), levels = 1:20))

# Result
#  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 
# 2  0  2  0  1  0  2  0  2  2  1  1  0  3  1  1  1  0  0 
# 20 
# 1





######################################################################

# Example of bootstrap with package caret and supervized classification


# Load needed library
library(caret)


# load the iris dataset
data(iris)

# define training control
# savepredictions allows to save predictions that are done on the out of bag data (OOB)
train_control <- trainControl(method="boot", number=100,
                              savePredictions = TRUE)

# Train the model with train function 
model <- train(Species~., data=iris, trControl=train_control, method="nb")


# Warning in FUN(X[[i]], ...): Numerical 0 probability for all classes with
## observation 25

# summarize results
print(model)
# Result 
## Naive Bayes
##
## 150 samples
## 4 predictor
## 3 classes: 'setosa', 'versicolor', 'virginica'
##
## No pre-processing
## Resampling: Bootstrapped (100 reps)
## Summary of sample sizes: 150, 150, 150, 150, 150, 150, ...
## Resampling results across tuning parameters:
##
## usekernel Accuracy Kappa
## FALSE 0.9507128 0.9253605
## TRUE 0.9494333 0.9234548
##
## Tuning parameter 'fL' was held constant at a value of 0
## Tuning
## parameter 'adjust' was held constant at a value of 1
## Accuracy was used to select the optimal model using the largest value.
## The final values used for the model were fL = 0, usekernel = FALSE and adjust
## = 1.


# We generate 100 bootstrap samples which will help us to chose between different tuning possible parameters.
# The considered tuning parameter here with naive bayes method is to considerate a parametric or non parametric estimator
# We have the accuracy and the kappa to evaluate the performance 
# For each bootstrap sample the model is adjusted on the bootstrap sample

# We can check the output
head(model$resample)
##       Accuracy Kappa Resample
## 1 0.9491525 0.9232437 Resample003
## 2 0.9444444 0.9160622 Resample004
## 3 0.9433962 0.9151547 Resample005
## 4 0.9433962 0.9150187 Resample006
## 5 0.9636364 0.9452736 Resample007
## 6 0.9444444 0.9131833 Resample001

# Each rows give the obtained performance on a bootstrap sample (resample)
# Look at the metrics distribution on the different re samples
resampleHist(model)





##########################################################################"


# Random forest

# Bootstrap allows to determine training samples on which each tree will be created
# Variable to test for each node in the tree construction are determined by a random drawn governed by mtry parameter

# Install needed package
install.packages("randomForest")
# Load needed library
library(randomForest)

# Use of randomForest on iris data
iris.rf = randomForest(Species ~ ., data = iris)

# By default, 500 trees (bootstrap samples) are considerated

# head of iris.rf$err.rate
head(iris.rf$err.rate)
# Column OOB indicates the error rate which as been done on missing data in the bootstrap sample


# Number of times when each person was outside the sample
iris.rf$oob.times
# Values with order of 1/3 * 500 (for each tree, one person has 1 chance over 3 to be absent of the training data)


# Probability of class affiliation "without bias" calculated from OOB samples
head(iris.rf$votes)
# For each person we calculate the frequency of the different class where she's affected in the different samples where she's out of bag and we divide by the total number of times when she's OOB







################################################################


# Linear regression on mtcars data

# Install needed package
install.packages("tidyverse")

# Load needed library
library(tidyverse)

# mtcars data
data("mtcars")


# Get to know mtcars data
#glimpse(mtcars) 
# or we can use ?mtcars
?mtcars

# Linear model on mtcars data stored in lm_mtcars variable
lm_mtcars = lm(mpg ~. , data = mtcars)

lm_mtcars

# Result

# Coefficients:
# (Intercept)          cyl         disp           hp  
# 12.30337     -0.11144      0.01334     -0.02148  
# drat           wt         qsec           vs  
# 0.78711     -3.71530      0.82104      0.31776  
# am         gear         carb  
# 2.52023      0.65541     -0.19942  

# Extract model coefficient with coef function
beta_hat_lm = coef(lm_mtcars)

beta_hat_lm

# Result
# (Intercept)         cyl        disp          hp 
# 12.30337416 -0.11144048  0.01333524 -0.02148212 
# drat          wt        qsec          vs 
# 0.78711097 -3.71530393  0.82104075  0.31776281 
# am        gear        carb 
# 2.52022689  0.65541302 -0.19941925 



# Square root with sqrt() function stored in sigma_hat variable
sigma_hat = sqrt(sum(lm_mtcars$residuals^2)/lm_mtcars$df.residual)

sigma_hat
# Result
# 2.650197



#  Studentized residuals of lm_mtcars with rstudent() function
res_stu_lm = rstudent(lm_mtcars)
res_stu_lm 

# predict the values based on the previous data (lm_mtcars) behaviors and thus we fit that data to the model
X_i_hat_beta = predict(lm_mtcars)
X_i_hat_beta




########################################################################

# Error sampling using mtcars data and lm_mtcars the model that we have fitted before
# Bootstrap of the errors on lm_mtcars will allow to obtain an approximative distribution of the coefficients estimator
data = mtcars
formule = formula(mpg~.)
model_init = lm_mtcars
B = 200
n = nrow(data)

# Beta that we have obtained by bootstraping the errors
beta_boot_errors = matrix(nrow=B,ncol=length(beta_hat_lm))


for (b in 1:B){
  # index for the bootstrap
  s_b = sample(1:n,replace = TRUE)
  # resampling the studentized residuals
  res_boot = res_stu_lm[s_b]
  # We recreate the bootstraped y : ajusted value + sigma * studentized residuals
  y_boot = X_i_hat_beta + sigma_hat*res_boot
  # dataset with bootstraped values of y
  data_boot = data
  data_boot$mpg = y_boot
  # We adjust the model on this bootstrap sample
  beta_boot_errors[b,] = coef(lm(formule,data=data_boot))
}



# Create function to calculate confidence intervals and percentiles
IC_basic_percentile=function(values_boot,value_hat,a=0.05){
  B = length(values_boot)
  sort_value_boot = sort(values_boot)
  binf_basic = 2*value_hat - sort_value_boot[ceiling(B*(1-a/2))]
  bsup_basic = 2*value_hat - sort_value_boot[ceiling(B*a/2)]
  binf_perc = sort_value_boot[ceiling(B*a/2)]
  bsup_perc = sort_value_boot[ceiling(B*(1-a/2))]
  bornes = c(binf_basic,bsup_basic,binf_perc,bsup_perc)
  names(bornes) = c("binf_basic","bsup_basic","binf_perc","bsup_perc")
  return(bornes)
}


# Use of IC_basic_percentile function
j = 5
IC_basic_percentile(beta_boot_errors[,j],beta_hat_lm[j],a=0.05)
# Result 
# binf_basic bsup_basic  binf_perc  bsup_perc 
# -2.285520   4.367453  -2.793231   3.859742 







###########################################################################"

# Test to know the variable benefit 



# Summary of data 
summary(lm_mtcars)

# Result
# Call:
lm(formula = mpg ~ ., data = mtcars)

# Residuals:
#   Min      1Q  Median      3Q     Max 
#   -3.4506 -1.6044 -0.1196  1.2193  4.6271 

# Coefficients:
#   Estimate Std. Error t value Pr(>|t|)  
# (Intercept) 12.30337   18.71788   0.657   0.5181  
# cyl         -0.11144    1.04502  -0.107   0.9161  
# disp         0.01334    0.01786   0.747   0.4635  
# hp          -0.02148    0.02177  -0.987   0.3350  
# drat         0.78711    1.63537   0.481   0.6353  
# wt          -3.71530    1.89441  -1.961   0.0633 .
# qsec         0.82104    0.73084   1.123   0.2739  
# vs           0.31776    2.10451   0.151   0.8814  
# am           2.52023    2.05665   1.225   0.2340  
# gear         0.65541    1.49326   0.439   0.6652  
# carb        -0.19942    0.82875  -0.241   0.8122  
# ---
#  Signif. codes:  
#  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

# Residual standard error: 2.65 on 21 degrees of freedom
# Multiple R-squared:  0.869,	Adjusted R-squared:  0.8066 
# F-statistic: 13.93 on 10 and 21 DF,  p-value: 3.793e-07



# Define j value = 2
j=2

# Using anova function on lm_mtcars stored in F_hat variable
# Analysis of Variance (ANOVA) is a statistical technique, commonly used to studying differences between two or more group means
F_hat = anova(lm_mtcars, lm( mpg~.-cyl, data = mtcars))

# Define B = 200
B = 200

# Store mtcars in the variable mtcars_boot_b
mtcars_boot_b = mtcars


# Adjustment of the model with null hypothesis H0 (without cyl variable)
# Use of lm function on mtcars stored in lm_mtcars_H0
lm_mtcars_H0 = lm(mpg ~.-cyl , data = mtcars)
lm_mtcars_H0

# Use of coef function on lm_mtcats_H0 stored in beta_hat_lm_H0
beta_hat_lm_H0 = coef(lm_mtcars_H0)
beta_hat_lm_H0

# Square root with sqrt() function stored in sigma_hat_H0
sigma_hat_H0 = sqrt(sum(lm_mtcars_H0$residuals^2)/lm_mtcars_H0$df.residual)
sigma_hat_H0
# Result
# 2.589966




# Studentized residuals of lm_mtcars_H0 with rstudent() function
res_stu_lm_H0 = rstudent(lm_mtcars_H0)
res_stu_lm_H0


# predict the values based on the previous data (lm_mtcars_H0) behaviors and thus we fit that data to the model
X_i_hat_beta_H0 = predict(lm_mtcars_H0)
X_i_hat_beta_H0

# Use of rep() method that takes a vector as an argument and returns the replicated values
F_hat_boot_errors_H0 = rep(0,B)
F_hat_boot_errors_H0




for (b in 1:B){
  s_b = sample(1:n,replace = TRUE)
  res_boot = res_stu_lm_H0[s_b]
  y_boot = X_i_hat_beta_H0 + sigma_hat_H0*res_boot
  data_boot = data
  data_boot$mpg = y_boot
  # Adjustment of the model with alternative hypothesis H1
  lm_class_boot_b = lm( mpg ~. , data = data_boot)
  # # Adjustment of the model with null hypothesis H0
  lm_class_boot_b_sans_cyl = lm( mpg ~. - cyl , data = data_boot)
  # Calculation of the test statistic for each bootstrap sample
  F_hat_boot_errors_H0[b] = anova(lm_class_boot_b,
                                  lm_class_boot_b_sans_cyl)$F[2]
}



# Calculation of the frequency at which the test statistic is bootstrapped
p_value_boot = mean(F_hat_boot_errors_H0 > F_hat$F[2])
p_value_boot
# Result 
# 0.91

#
p_value_boot_v2 = (sum(F_hat_boot_errors_H0 > F_hat$F[2]) + 1) / (B+1)
p_value_boot_v2
# Result 
# 0.9104478

# Print both result together
print(c(p_value_boot,p_value_boot_v2))
# 0.9100000 0.9104478
# It's higher than the initial valus

# Plot of lm_mtcars with CDF under H0 on the y axis and seq on the x axis
plot(seq(0,5,0.01),pf(seq(0,5,0.01),df2=lm_mtcars$df.residual,df1=1),ylab="CDF under H0",type="l",col="green")

lines(sort(F_hat_boot_errors_H0),(1/B)*(1:B),type="l",col="red")
# We see that the boostrapped statistic is close to the expected distribution of the test statistic under H0 (in the case of a gaussian model)






#########################################################################


# Confidence intervals with logistic regression 


# Load data
urine = read.table(file='/Users/mario/Desktop/Dekstop_file/M2 Health Data Science 2021 2022/Mthodes numriques/Vincent Vanderwalle/Document/Introduction au Bootstrap/urine.dat',header=T)


# See the data
glimpse(urine)

## Rows: 79
## Columns: 7
## $ r <int> 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,~
## $ gravity <dbl> 1.021, 1.017, 1.008, 1.011, 1.005, 1.020, 1.012, 1.029, 1.015,~
## $ ph <dbl> 4.91, 5.74, 7.20, 5.51, 6.52, 5.27, 5.62, 5.67, 5.41, 6.13, 6.~
## $ osmo <int> 725, 577, 321, 408, 187, 668, 461, 1107, 543, 779, 345, 907, 2~
## $ cond <dbl> NA, 20.0, 14.9, 12.6, 7.5, 25.3, 17.4, 35.9, 21.9, 25.7, 11.5,~
## $ urea <int> 443, 296, 101, 224, 91, 252, 195, 550, 170, 382, 152, 448, 64,~
## $ calc <dbl> 2.45, 4.49, 2.36, 2.15, 1.16, 3.34, 1.40, 8.48, 1.16, 2.21, 1.~


# Missing values for osmo and cond
colSums(is.na(urine))

## r gravity ph osmo cond urea calc
## 0 0 0 1 1 0 0



# Imputation of missing values by the median
urine$osmo[which(is.na(urine$osmo))] = median(urine$osmo,na.rm = TRUE)
urine$cond[which(is.na(urine$cond))] = median(urine$cond,na.rm = TRUE)


# Summary of the data urine
summary(urine)

## r gravity ph osmo
## Min. :0.0000 Min. :1.005 Min. :4.760 Min. : 187.0
## 1st Qu.:0.0000 1st Qu.:1.012 1st Qu.:5.530 1st Qu.: 413.0
## Median :0.0000 Median :1.018 Median :5.940 Median : 612.5
## Mean :0.4304 Mean :1.018 Mean :6.028 Mean : 615.0
## 3rd Qu.:1.0000 3rd Qu.:1.024 3rd Qu.:6.385 3rd Qu.: 792.0
## Max. :1.0000 Max. :1.040 Max. :7.940 Max. :1236.0
## cond urea calc
## Min. : 5.10 Min. : 10.0 Min. : 0.170
## 1st Qu.:14.45 1st Qu.:160.0 1st Qu.: 1.460
## Median :21.40 Median :260.0 Median : 3.160
## Mean :20.91 Mean :266.4 Mean : 4.139
## 3rd Qu.:26.55 3rd Qu.:372.0 3rd Qu.: 5.930
## Max. :38.00 Max. :620.0 Max. :14.340


# Adjustment of a logistic regression to predict r variable according to the other variables

# Use of glm function on urine data
glm_class = glm(r ~. ,data =urine,family="binomial")
glm_class
# Result
# Coefficients:
# (Intercept)      gravity           ph         osmo  
# -5.242e+02    5.234e+02   -3.298e-01   -1.866e-03  
# cond         urea         calc  
# -1.901e-01   -1.657e-02    7.229e-01  

# Degrees of Freedom: 78 Total (i.e. Null);  72 Residual
# Null Deviance:	    108 
# Residual Deviance: 60.36 	AIC: 74.36


# Use of coef function on glm_class
beta_hat_glm = coef(glm_class)
beta_hat_glm
# Result
#   (Intercept)       gravity            ph          osmo 
# -524.22242572  523.38052177   -0.32984083   -0.00186588 
# cond          urea          calc 
# -0.19013389   -0.01657303    0.72289054 

# Use of residuals function on glm_class
res_glm = residuals(glm_class,type="deviance")
res_glm

# Use of predict function on glm_class
pi_hat = predict(glm_class,type="response")
pi_hat







#############################################################################

# Error sampling with urine data 

# Define data
data = urine

# Define formula
formule = formula(r~.)

# Define model with glm function applied on urine data
model_init = glm(formule,family="binomial",data)
model_init
# Result
# Coefficients:
# (Intercept)      gravity           ph         osmo  
# -5.242e+02    5.234e+02   -3.298e-01   -1.866e-03  
# cond         urea         calc  
# -1.901e-01   -1.657e-02    7.229e-01  

# Degrees of Freedom: 78 Total (i.e. Null);  72 Residual
# Null Deviance:	    108 
# Residual Deviance: 60.36 	AIC: 74.36

# Use of coef function of model_init
beta_hat = coef(model_init)
beta_hat
# Result
#   (Intercept)       gravity            ph          osmo 
# -524.22242572  523.38052177   -0.32984083   -0.00186588 
# cond          urea          calc 
# -0.19013389   -0.01657303    0.72289054 

# Use of residuals function on model_init
residus = residuals(model_init,type="pearson")
residus
# We did it with pearson residuals but we can also do it with  residus = residuals(model_init,type="deviance")

# Define B and n
B = 200
n = nrow(data)

# Use of matrix function stored in beta_boot_errors
beta_boot_errors = matrix(nrow=B,ncol=length(beta_hat))


for (b in 1:B){
  s_b = sample(1:n,replace = TRUE)
  # Resampling of the residuals
  res_boot = residus[s_b]
  # We deduce the psi_boot (from which we deduce the y_boot afterwards)
  psi_boot = model_init$fitted.values +
    sqrt(model_init$fitted.values*(1-model_init$fitted.values))*res_boot
  # We deduce the y_boot
  y_boot = ifelse(psi_boot < 1/2, 0, 1)
  data_boot = data
  data_boot$r = y_boot
  # We adjust the model on the bootstrapped data
  beta_boot_errors[b,] = coef(glm(formule,data=data_boot,family="binomial"))
}


# Use of rbind function on beta_hat
rbind(beta_hat, apply(beta_boot_errors, 2, quantile))












